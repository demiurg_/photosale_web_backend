package rs.raf.web.webproj.service;

import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.QueryParam;

import rs.raf.web.webproj.domain.User;
import rs.raf.web.webproj.domain.dto.ChangePasswordDto;
import rs.raf.web.webproj.domain.dto.ImageDownloadInfo;
import rs.raf.web.webproj.domain.dto.LoginRequestDto;
import rs.raf.web.webproj.domain.dto.LoginResponseDto;
import rs.raf.web.webproj.domain.dto.TestUser;
import rs.raf.web.webproj.domain.dto.UserRegistrationDto;

public interface UserService {
	
	public List<User> findAll ();
	
	//public List<User> findAllDeletedUsers ();
	
	public List<User> findByApprovedStatus (Integer status);
	
	public Boolean companyApproveUser (Integer userId, Integer companyId);
	
	public User register (UserRegistrationDto userRegistrationDto);
	
	public LoginResponseDto login (LoginRequestDto loginRequestDto);
	
	public Boolean newSaller (Integer userId);
	
	public Boolean verify (String token);
	
	public Boolean forgotPassword (String email);
	
	public Boolean checkUploadLimit (Integer userId);
	
	public Boolean addCreditCard (Integer userId, String card);
	
	public Boolean changePassword (ChangePasswordDto changePasswordDto);
	
	public List<TestUser> findTestUsers ();
	
	public boolean sendPurchasedPhotoToUserMail(ImageDownloadInfo imageDownloadInfo) throws MessagingException;
	
}
