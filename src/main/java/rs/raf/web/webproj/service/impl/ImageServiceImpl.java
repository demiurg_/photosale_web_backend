package rs.raf.web.webproj.service.impl;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.codec.language.bm.PhoneticEngine;

import rs.raf.web.webproj.dao.CartImageDao;
import rs.raf.web.webproj.dao.CategoryDao;
import rs.raf.web.webproj.dao.ImageCategoryDao;
import rs.raf.web.webproj.dao.ImageDao;
import rs.raf.web.webproj.dao.RatingDao;
import rs.raf.web.webproj.dao.impl.CartImageDaoImpl;
import rs.raf.web.webproj.dao.impl.CategoryDaoImpl;
import rs.raf.web.webproj.dao.impl.ImageCategoryDaoImpl;
import rs.raf.web.webproj.dao.impl.ImageDaoImpl;
import rs.raf.web.webproj.dao.impl.RatingDaoImpl;
import rs.raf.web.webproj.domain.CartImage;
import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.ImageCategory;
import rs.raf.web.webproj.domain.Rating;
import rs.raf.web.webproj.domain.Resolution;
import rs.raf.web.webproj.domain.dto.FullImageDto;
import rs.raf.web.webproj.domain.dto.ResolutionDto;
import rs.raf.web.webproj.domain.dto.UploadImageDto;
import rs.raf.web.webproj.service.ImageService;
import rs.raf.web.webproj.service.ImageUtils;

public class ImageServiceImpl implements ImageService {
	
	private ImageDao imageDao;
	
	private CartImageDao cartImageDao;
	
	private RatingDao ratingDao;
	
	private CategoryDao categoryDao;
	
	private ImageCategoryDao imageCategoryDao;
	
	public ImageServiceImpl() {
		imageDao = new ImageDaoImpl();
		cartImageDao = new CartImageDaoImpl();
		ratingDao = new RatingDaoImpl();
		imageCategoryDao = new ImageCategoryDaoImpl();
		categoryDao = new CategoryDaoImpl();
	}

	@Override
	public Image upload(UploadImageDto uploadImageDto) {
		
		//proveri test deo i count deo
		
		// TODO: Mihalo, pomagaj!!!
		Image image = insertImage(uploadImageDto);
		
		saveResolutions(uploadImageDto.getResolutionList(),image);
		
		categoryDao.addCategoriesToPhoto(image.getId(),uploadImageDto.getCategoryList());
		
		return null;
	}

	private void saveResolutions(List<ResolutionDto> resolutionList, Image image) {
		for(ResolutionDto resolutionDto : resolutionList) {
			
			String path = writeImagesToDisk(resolutionDto);
			Resolution resolution = new Resolution(image.getId(),resolutionDto.getResolution(),path,0,resolutionDto.getPrice());
			imageDao.saveResolution(resolution);
		}
		
	}

	private Image insertImage(UploadImageDto uploadImageDto) {
		Image image = new Image();
		//informacije iz dto obj
		image.setName(uploadImageDto.getName());
		image.setLocation(uploadImageDto.getLocation());
		image.setUserId(uploadImageDto.getUserId());
		image.setDeleted(0);
		//dodatne info koje znamo o slici
		image.setApprovedStatus(0);
		image.setDateOfUpload(new Date());
		//za cenu mu stavljam cenu jedne od rezolucija
		image.setPrice(uploadImageDto.getResolutionList().get(0).getPrice());
		image = imageDao.save(image);
		//uzmes jednu rezolucije, cropujes je, stavis joj watermark, posaljes je da se upise na disk...
		//na kraju se vraca path do kreiranog fajla slike koja se prikazuje na sajtu (zvacemo je thumb/public/slika sa zigom)
		//taj path cuvamo u image
		image.setPath(prepareImage(uploadImageDto.getResolutionList()));
		imageDao.updatePath(image);
		return image;
	}

	private String prepareImage(List<ResolutionDto> resolutionList) {
		ResolutionDto r = resolutionList.get(0);
		//U zavisnosti od tipa rezolucije, slika se smesta u root folder( ako je tip "public")
		// ili u folder resolutions (ukoliko je navedena rezolucija npr "1280 x 720")
		r.setResolution("public");
		//pre nego sto je upisemo na disk moramo da je smanjimo. U ovoj metodi cemo i dodati watermark na sliku
		r.setBase64(cropImage(r));
		
		return writeImagesToDisk(r);
	}

	private String cropImage(ResolutionDto r) {
		byte[] bytes = base64ToByte(r.getBase64());
		ImageUtils imgUtils = new ImageUtils();
		BufferedImage buffImg = null;
		BufferedImage scaleBuffImg = null;

		try {
			buffImg = imgUtils.cropImageSquare(bytes);
			scaleBuffImg = imgUtils.resizeImage(buffImg, 450, 450);
		} catch (IOException e) {
			e.printStackTrace();
		}

		BufferedImage addedWatermark = addWatermark("ML", scaleBuffImg);

		String base64 = imgUtils.base64FromImage(addedWatermark);
		return base64;
	}

	private String writeImagesToDisk(ResolutionDto r) {
		String finalString = "";
		if (r.getResolution().equals("public")) {
			File theDir = new File("/var/www/html/images");
			if (!theDir.exists()) {
				System.out.println("DIR doesn't exist, create a new one " + theDir.getName());
				boolean result = false;
				try {
					theDir.mkdir();
					result = true;
				} catch (SecurityException se) {
				}
				if (result) {
					System.out.println("DIR created");
				}
			}
			finalString = "/var/www/html/images/" + r.getName() + "-"
					+ r.getResolution() + ".png";
			createImageFile(r, "");
		} else {
			String foldername = "";
			foldername = "resolutions";

			File theDir = new File("/var/www/html/images/" + foldername);

			if (!theDir.exists()) {
				System.out.println("creating directory: " + theDir.getName());
				boolean result = false;
				try {
					theDir.mkdir();
					result = true;
				} catch (SecurityException se) {
				}
				if (result) {
					System.out.println("DIR created");
				}
			}
			createImageFile(r, foldername);
			finalString = "/var/www/html/images/" + foldername + "/" + r.getName()
					+ "-" + r.getResolution() + ".png";
		}

		return finalString;
	}
	private void createImageFile(ResolutionDto resolution, String foldername) {
		ImageUtils imgUtils = new ImageUtils();
		BufferedImage img = imgUtils.imageFromBase64(resolution.getBase64());
		String name = resolution.getName() + "-" + resolution.getResolution();

		File outputfile = null;

		if (foldername.equals("")) {
			outputfile = new File("/var/www/html/images/" + name + ".png");
//			System.out.println("116->"+outputfile.getPath());
		} else {
			outputfile = new File("/var/www/html/images/" + foldername + "/" + name + ".png");
			System.out.println(outputfile.getName());
		}
		try {
			ImageIO.write(img, "png", outputfile);
		} catch (IOException e) {
			System.out.println("Failed creating file for image");
			e.printStackTrace();
		}
	}
	
	private BufferedImage addWatermark(String text, BufferedImage sourceImage) {
		 Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
		 
			// initializes necessary graphic properties
			AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
			g2d.setComposite(alphaChannel);
			g2d.setColor(Color.WHITE);
			g2d.setFont(new Font("Arial", Font.BOLD, 124));
			FontMetrics fontMetrics = g2d.getFontMetrics();
			Rectangle2D rect = fontMetrics.getStringBounds(text, g2d);
	 
			// calculates the coordinate where the String is painted
			int centerX = (sourceImage.getWidth() - (int) rect.getWidth()) / 2;
			int centerY = sourceImage.getHeight() / 2;
	 
			// paints the textual watermark
			g2d.drawString(text, centerX, centerY);
			g2d.drawString(text, centerX, centerY);
			g2d.drawString(text, centerX, centerY);
			g2d.drawString(text, centerX, centerY);
	 
//		        ImageIO.write(sourceImage, "png", destImageFile);
			g2d.dispose();
	 
			System.out.println("Slika je zigosana. Kkk");
			return sourceImage;
	}
	
	//pomocna funkcija koja nedostaje u ImageUtils class-i
	public byte[] base64ToByte(String base64) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageUtils imgUtils = new ImageUtils();
		BufferedImage img = imgUtils.imageFromBase64(base64);
		try {
			ImageIO.write(img, "jpg", baos);
		} catch (IOException e) {
			System.out.println("base64 to byte arr failed");
			e.printStackTrace();
		}
		byte[] bytes = baos.toByteArray();
		return bytes;
	}

	@Override
	public List<Image> findAll() {
		return imageDao.findAll();
	}

	@Override
	public List<Image> findByApprovedStatus(Integer status, int page, int size) {
		return imageDao.findByApprovedStatus(status, page, size);
	}

	@Override
	public List<FullImageDto> findByUser(Integer userId) {
		List<Image> imagesOfUser = imageDao.findByUserId(userId);
		List<FullImageDto> data = new ArrayList<>();
		for(Image image: imagesOfUser) {
			FullImageDto fullImage = imageDao.findById(image.getId());
			if(fullImage != null) {
				data.add(fullImage);
			}
		}
		return data;
	}

	@Override
	public List<Image> findByCategory(Integer categoryId) {
		List<Image> images = new ArrayList<>();
		List<ImageCategory> imageCategories = imageCategoryDao.findByCategoryId(categoryId);
		for(ImageCategory imageCategory: imageCategories) {
			Image image = imageDao.findByImageId(imageCategory.getImageId());
			if(image != null) {
				images.add(image);
			}
		}
		return images;
	}

	@Override
	public Boolean approveImage(Integer imageId) {
		return imageDao.updateApprovedStatus(imageId, 1);
	}

	@Override
	public Boolean deleteImage(Integer imageId) {
		return imageDao.updateDeletedStatus(imageId, 1);
	}

	@Override
	public List<Image> searchImages(Integer size, Integer page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean addToCart(Integer imageId, Integer cartId) {
		CartImage cartImage = new CartImage();
		cartImage.setCartId(cartId);
		cartImage.setImageId(imageId);
		cartImage.setResolution("4K");
		cartImageDao.save(cartImage);
		return true;
	}

	@Override
	public Boolean rateImage(Integer imageId, Integer userId, Integer rating) {
		Rating ratingObject = new Rating();
		ratingObject.setImageId(imageId);
		ratingObject.setUserId(userId);
		ratingObject.setRating(rating);
		ratingDao.save(ratingObject);
		return true;
	}

	@Override
	public Boolean removeFromCart(Integer imageId, Integer cartId) {
		return imageDao.removeFromCart(imageId, cartId);
	}

	@Override
	public Double fetchRating(Integer imageId) {
		List<Rating> ratings = imageDao.findRatingsByImageId(imageId);
		double value = 0;
		for(Rating r: ratings) value += r.getRating();
		return (value / ratings.size());
	}

}
