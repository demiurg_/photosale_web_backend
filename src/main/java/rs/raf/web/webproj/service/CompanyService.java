package rs.raf.web.webproj.service;

import java.util.List;

import rs.raf.web.webproj.domain.Company;

public interface CompanyService {
	
	public List<Company> findAll();
	
	public List<Company> findByApprovedStatus (Integer status);
	
	public Boolean joinCompany(Integer companyId, Integer userId);
	
	public Boolean approveCompany(Integer companyId);
	
	public Company registerCompany (Company company);

}
