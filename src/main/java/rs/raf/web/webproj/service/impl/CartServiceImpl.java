package rs.raf.web.webproj.service.impl;

import java.util.List;

import rs.raf.web.webproj.dao.CartDao;
import rs.raf.web.webproj.dao.impl.CartDaoImpl;
import rs.raf.web.webproj.domain.Cart;
import rs.raf.web.webproj.domain.ImageResolution;
import rs.raf.web.webproj.domain.dto.CartDto;
import rs.raf.web.webproj.service.CartService;

public class CartServiceImpl implements CartService {
	
	private CartDao cartDao;
	
	public CartServiceImpl() {
		cartDao = new CartDaoImpl();
	}

	@Override
	public List<Cart> findAll() {
		return cartDao.findAll();
	}

	@Override
	public List<CartDto> findByUser(Integer userId) {
		return cartDao.findByUser(userId);
	}

	@Override
	public Cart openCart(Integer userId) {
		Cart cart = new Cart();
		cart.setStatus(1);
		cart.setUserId(userId);
		cart.setVersion(0);
		return cartDao.save(cart);
	}

	@Override
	public Boolean closeCart(Integer cartId) {
		return cartDao.setStatus(cartId, 0);
	}

	@Override
	public CartDto findActiveCartByUser(Integer userId) {
		return cartDao.findActiveCart(userId);
	}

	@Override
	public List<ImageResolution> findContentByCart(Integer cartId) {
		// TODO Auto-generated method stub
		return null;
	}

}
