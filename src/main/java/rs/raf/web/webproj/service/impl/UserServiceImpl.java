package rs.raf.web.webproj.service.impl;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import rs.raf.web.webproj.dao.CartDao;
import rs.raf.web.webproj.dao.ImageDao;
import rs.raf.web.webproj.dao.UserDao;
import rs.raf.web.webproj.dao.impl.CartDaoImpl;
import rs.raf.web.webproj.dao.impl.ImageDaoImpl;
import rs.raf.web.webproj.dao.impl.UserDaoImpl;
import rs.raf.web.webproj.domain.User;
import rs.raf.web.webproj.domain.dto.ChangePasswordDto;
import rs.raf.web.webproj.domain.dto.ImageDownloadInfo;
import rs.raf.web.webproj.domain.dto.LoginRequestDto;
import rs.raf.web.webproj.domain.dto.LoginResponseDto;
import rs.raf.web.webproj.domain.dto.TestUser;
import rs.raf.web.webproj.domain.dto.UserRegistrationDto;
import rs.raf.web.webproj.service.SecurityService;
import rs.raf.web.webproj.service.UserService;

public class UserServiceImpl implements UserService {
	
	private static final Integer DELETED_STATUS = 1;
	
	private UserDao userDao;
	
	private CartDao cartDao;
	
	private SecurityService securityService;
	
	private ImageDao imageDao;
	
	public UserServiceImpl() {
		userDao = new UserDaoImpl();
		cartDao = new CartDaoImpl();
		imageDao = new ImageDaoImpl();
	}

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}

	@Override
	public Boolean checkUploadLimit(Integer userId) {

		User user = userDao.findById(userId);
		Date userLimitDate = user.getLimitDate();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        
        if(now.before(userLimitDate)) {
        	int dayCount = user.getDailyCount();
        	int weekCount = user.getWeeklyCount();
        	if(dayCount < 3 && weekCount < 8) {
        		return true;
        	}else {
        		return false;
        	}
        }else {
        	cal.setTime(now);
			cal.add(Calendar.DAY_OF_MONTH, 7);
			Date newDateLimitForUser = cal.getTime();
			userDao.resetUploadLimit(userId, newDateLimitForUser);
        }
		return true;
	}

	@Override
	public List<TestUser> findTestUsers() {
		return userDao.findTestUsers();
	}

	@Override
	public User register(UserRegistrationDto userRegistrationDto) {
		User user = new User();
		user.setUsername(userRegistrationDto.getUsername());
		user.setEmail(userRegistrationDto.getEmail());
		user.setDeleted(0);
		user.setLimitDate(new Date());
		user.setVerify(0);
		user.setPassword(userRegistrationDto.getPassword());
		user.setRole(0);
		userDao.save(user);
		rs.raf.web.webproj.domain.TestUser testUser = new rs.raf.web.webproj.domain.TestUser();
		testUser.setUserId(user.getId());
		testUser.setCount(0);
		userDao.saveTestUser(testUser);
		// TODO: Poslati mail sa tokenom
		return user;
	}

	@Override
	public LoginResponseDto login(LoginRequestDto loginRequestDto) {
		User user = userDao.findByEmailAndPassword(loginRequestDto.getEmail(), loginRequestDto.getPassword());
		if(user == null) return null;
		LoginResponseDto dto = new LoginResponseDto();
		dto.setUser(user);
		Integer cartId = cartDao.activeCartId(user.getId());
		dto.setActiveCartId(cartId);
		return dto;
	}

	@Override
	public Boolean verify(String token) {
		String email = securityService.decryptEmail(token);
		if(email == null) return false;
		User user = userDao.findByEmail(email);
		return userDao.updateVerifyStatus(user.getId(), 1);
	}

	@Override
	public Boolean companyApproveUser(Integer userId, Integer companyId) {
		return userDao.setCompanyIdToUser(userId, companyId);
	}

	@Override
	public List<User> findByApprovedStatus(Integer status) {
		return userDao.findByApprovedStatus(status);
	}

	@Override
	public Boolean newSaller(Integer userId) {
		boolean res = userDao.updateRole(userId, 1);
		if(res) userDao.deleteTestUser(userId);
		return true;
	}

	@Override
	public Boolean changePassword(ChangePasswordDto changePasswordDto) {
		return userDao.changePassword(changePasswordDto.getUserId(), changePasswordDto.getNewPassword());
	}

	@Override
	public Boolean forgotPassword(String email) {
		// TODO: Poslati random lozinku na mail
		return null;
	}

	@Override
	public Boolean addCreditCard(Integer userId, String card) {
		return userDao.setCreditCard(userId, card);
	}

	@Override
	public boolean sendPurchasedPhotoToUserMail(ImageDownloadInfo imageDownloadInfo) throws MessagingException {
		MailUtils mailUtils = new MailUtils();
		//samo unesi neki pravi email
		mailUtils.setSender("YOYOYO@gmail.com", "xoxoxoxo");
		mailUtils.addRecipient("YOYOYO@gmail.com");
		mailUtils.setSubject("Stigle slikeee");
		String path = "";
		path = imageDao.getResolutionPathForRequestedImage(imageDownloadInfo.getPhotoId(), imageDownloadInfo.getResolution());
		mailUtils.addAttachment(path);
		mailUtils.setBody("Image name:"+imageDownloadInfo.getName()+" , price: $"+imageDownloadInfo.getPrice()+" , image resolution: "+imageDownloadInfo.getResolution());
		mailUtils.send();
		return true;
	}

}
