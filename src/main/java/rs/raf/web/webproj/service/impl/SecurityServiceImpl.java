package rs.raf.web.webproj.service.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import rs.raf.web.webproj.service.SecurityService;

public class SecurityServiceImpl implements SecurityService {
	
	private static final String KEY = "vEsNiKlEtNjEDuGoDnEvNiCe";

	@Override
	public String encryptEmail(String email) {
		return Jwts.builder().setSubject(email).signWith(SignatureAlgorithm.HS512, KEY).compact();
	}

	@Override
	public String decryptEmail(String token) {
		Claims claims = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token).getBody();
		return claims.getSubject();
	}

}
