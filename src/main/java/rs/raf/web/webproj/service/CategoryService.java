package rs.raf.web.webproj.service;

import java.util.List;

import rs.raf.web.webproj.domain.Category;

public interface CategoryService {
	
	public List<Category> findAll();
	
	public List<Category> findCategoriesForImage(Integer imageId);
	
	public Category createNew (String name);
	
	public boolean addCategoryToPhoto(long photoId, List<Integer> list);

}
