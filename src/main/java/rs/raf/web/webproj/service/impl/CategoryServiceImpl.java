package rs.raf.web.webproj.service.impl;

import java.util.List;

import rs.raf.web.webproj.dao.CategoryDao;
import rs.raf.web.webproj.dao.impl.CategoryDaoImpl;
import rs.raf.web.webproj.domain.Category;
import rs.raf.web.webproj.service.CategoryService;

public class CategoryServiceImpl implements CategoryService {
	
	private CategoryDao categoryDao;
	
	public CategoryServiceImpl() {
		categoryDao = new CategoryDaoImpl();
	}

	@Override
	public List<Category> findAll() {
		return categoryDao.findAll();
	}

	@Override
	public List<Category> findCategoriesForImage(Integer imageId) {
		return categoryDao.findByImageId(imageId);
	}

	@Override
	public Category createNew (String name) {
		Category category = new Category();
		category.setName(name);
		return categoryDao.save(category);
	}

	@Override
	public boolean addCategoryToPhoto(long photoId, List<Integer> list) {
		return categoryDao.addCategoriesToPhoto(photoId, list);
	}

}
