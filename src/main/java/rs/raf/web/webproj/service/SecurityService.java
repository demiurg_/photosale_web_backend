package rs.raf.web.webproj.service;

public interface SecurityService {
	
	public String encryptEmail (String email);
	
	public String decryptEmail (String token);

}
