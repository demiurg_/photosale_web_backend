package rs.raf.web.webproj.service;

import java.util.List;

import rs.raf.web.webproj.domain.Cart;
import rs.raf.web.webproj.domain.ImageResolution;
import rs.raf.web.webproj.domain.dto.CartDto;

public interface CartService {
	
	public List<Cart> findAll();
	
	public List<CartDto> findByUser(Integer userId);
	
	public Cart openCart(Integer userId);
	
	public Boolean closeCart(Integer cartId);
	
	public CartDto findActiveCartByUser (Integer userId);
	
	public List<ImageResolution> findContentByCart(Integer cartId);

}
