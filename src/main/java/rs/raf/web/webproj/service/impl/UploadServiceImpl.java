package rs.raf.web.webproj.service.impl;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import rs.raf.web.webproj.service.ImageUtils;
import rs.raf.web.webproj.service.UploadService;

public class UploadServiceImpl implements UploadService {

	private static final String WATERMARK_TEXT = "RAF";

	@Override
	public String uploadImage(String base64Image, boolean crop, boolean watermark) {
		// TODO Auto-generated method stub
		return null;
	}

	private String cropImageAndSetWatermark(String base64Image) {
		byte[] bytes = base64ToByte(base64Image);
		ImageUtils imgUtils = new ImageUtils();
		BufferedImage buffImg = null;
		BufferedImage scaleBuffImg = null;

		try {
			buffImg = imgUtils.cropImageSquare(bytes);
			scaleBuffImg = imgUtils.resizeImage(buffImg, 450, 450);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// BufferedImage addedWatermark = addWatermark("X", scaleBuffImg);
		Graphics2D g2d = (Graphics2D) scaleBuffImg.getGraphics();

		// initializes necessary graphic properties
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
		g2d.setComposite(alphaChannel);
		g2d.setColor(Color.WHITE);
		g2d.setFont(new Font("Arial", Font.BOLD, 224));
		FontMetrics fontMetrics = g2d.getFontMetrics();
		Rectangle2D rect = fontMetrics.getStringBounds(WATERMARK_TEXT, g2d);

		// calculates the coordinate where the String is painted
		int centerX = (scaleBuffImg.getWidth() - (int) rect.getWidth()) / 2;
		int centerY = scaleBuffImg.getHeight() / 2;

		// paints the textual watermark
		g2d.drawString(WATERMARK_TEXT, centerX, centerY);
		g2d.drawString(WATERMARK_TEXT, centerX, centerY);
		g2d.drawString(WATERMARK_TEXT, centerX, centerY);
		g2d.drawString(WATERMARK_TEXT, centerX, centerY);

		// ImageIO.write(sourceImage, "png", destImageFile);
		g2d.dispose();

		String base64 = imgUtils.base64FromImage(scaleBuffImg);
		return base64;
	}

	private byte[] base64ToByte(String base64) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageUtils imgUtils = new ImageUtils();
		BufferedImage img = ImageUtils.imageFromBase64(base64);
		try {
			ImageIO.write(img, "jpg", baos);
		} catch (IOException e) {
			System.out.println("base64 to byte arr failed");
			e.printStackTrace();
		}
		byte[] bytes = baos.toByteArray();
		return bytes;
	}

}
