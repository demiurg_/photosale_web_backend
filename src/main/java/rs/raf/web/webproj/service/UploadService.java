package rs.raf.web.webproj.service;

public interface UploadService {
	
	public String uploadImage (String base64Image, boolean crop, boolean watermark);

}
