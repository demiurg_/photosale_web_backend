package rs.raf.web.webproj.service;

import java.util.List;

import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.dto.FullImageDto;
import rs.raf.web.webproj.domain.dto.UploadImageDto;

public interface ImageService {
	
	public Image upload (UploadImageDto uploadImageDto);
	
	public List<Image> findAll();
	
	public List<Image> findByApprovedStatus(Integer status, int page, int size);
	
	public List<FullImageDto> findByUser(Integer userId);
	
	public List<Image> findByCategory(Integer categoryId);
	
	public Boolean approveImage (Integer imageId);
	
	public Boolean deleteImage (Integer imageId);
	
	public List<Image> searchImages (Integer size, Integer page);
	
	public Boolean addToCart (Integer imageId, Integer cartId);
	
	public Boolean removeFromCart (Integer imageId, Integer cartId);
	
	public Boolean rateImage (Integer imageId, Integer userId, Integer rating);
	
	public Double fetchRating (Integer imageId);

}
