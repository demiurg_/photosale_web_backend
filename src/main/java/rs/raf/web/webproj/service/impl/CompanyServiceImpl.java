package rs.raf.web.webproj.service.impl;

import java.util.List;

import rs.raf.web.webproj.dao.CompanyDao;
import rs.raf.web.webproj.dao.impl.CompanyDaoImpl;
import rs.raf.web.webproj.domain.Company;
import rs.raf.web.webproj.service.CompanyService;

public class CompanyServiceImpl implements CompanyService {
	
	private CompanyDao companyDao;
	
	public CompanyServiceImpl() {
		companyDao = new CompanyDaoImpl();
	}

	@Override
	public List<Company> findAll() {
		return companyDao.findAll();
	}
	
	@Override
	public List<Company> findByApprovedStatus(Integer status) {
		return companyDao.findByApprovedStatus(status);
	}

	@Override
	public Boolean joinCompany(Integer companyId, Integer userId) {
		return companyDao.joinUserToCompany(userId, companyId);
	}

	@Override
	public Boolean approveCompany(Integer companyId) {
		return companyDao.updateApprovedStatus(companyId, 1);
	}

	@Override
	public Company registerCompany(Company company) {
		return companyDao.save(company);
	}

}
