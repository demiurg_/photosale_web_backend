package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.CategoryDao;
import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.dao.ImageDao;
import rs.raf.web.webproj.domain.Category;
import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.ImageResolution;
import rs.raf.web.webproj.domain.Rating;
import rs.raf.web.webproj.domain.Resolution;
import rs.raf.web.webproj.domain.dto.FullImageDto;

public class ImageDaoImpl implements ImageDao {

	private Connection connection;

	private CategoryDao categoryDao = null;

	public ImageDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
		this.categoryDao = new CategoryDaoImpl();
	}

	@Override
	public List<Image> findAll() {
		try {
			List<Image> images = new ArrayList<>();
			String QUERY = "SELECT * FROM image";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rsI = preparedStatement.executeQuery();
			while (rsI.next()) {
				Image image = new Image();
				image.setId(rsI.getInt("id"));
				image.setApprovedStatus(rsI.getInt("approved_status"));
				image.setDateOfApproved(rsI.getDate("date_of_approved"));
				image.setDateOfUpload(rsI.getDate("date_of_upload"));
				image.setDeleted(rsI.getInt("deleted"));
				image.setName(rsI.getString("name"));
				image.setLocation(rsI.getString("location"));
				image.setPrice(rsI.getInt("price"));
				image.setRating(rsI.getDouble("rating"));
				image.setUserId(rsI.getInt("id"));
				image.setVersion(rsI.getInt("version"));
				image.setPath(rsI.getString("path"));
				images.add(image);
			}
			return images;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public List<Image> findAllFiltered(String input, String searchCriteria, String sortCriteria, String order) {
		String QUERY = "SELECT usr_img.user_id, usr_img.username, usr_img.img_name, usr_img.img_location, usr_img.date_of_upload, usr_img.price, usr_img.path, usr_img.img_rating, c.id company_id, c.name company_name, c.rang company_rang\n" + 
				"FROM\n" + 
				"(SELECT u.id user_id,u.username, u.company_id, i.name img_name, i.location img_location, i.date_of_upload, i.price, i.path, i.rating img_rating, i.deleted ,i.approved_status\n" + 
				"FROM web_exam.user u\n" + 
				"INNER JOIN web_exam.image i \n" + 
				"ON u.id = i.user_id) usr_img\n" + 
				"LEFT JOIN web_exam.comapany c\n" + 
				"ON usr_img.company_id = c.id\n" + 
				"WHERE usr_img.deleted = 0 AND usr_img.approved_status = 1 AND "+searchCriteria+" LIKE ?\n" + 
				"ORDER BY CASE WHEN company_rang = 1 THEN 0\n" + 
				"								WHEN usr_img.img_rating >= 4.0 AND usr_img.img_rating <= 5.0 THEN 1\n" + 
				"								WHEN company_rang = 1 THEN 2\n" + 
				"								WHEN usr_img.img_rating >= 3.0 AND usr_img.img_rating <= 4.0 THEN 3\n" + 
				"                                ELSE "+sortCriteria+"\n" + 
				"                                END, "+sortCriteria+" "+order;
		
		
		try {
			List<Image> images = new ArrayList<>();
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rsI = preparedStatement.executeQuery();
			while (rsI.next()) {
				Image image = new Image();
				image.setId(rsI.getInt("id"));
				image.setApprovedStatus(rsI.getInt("approved_status"));
				image.setDateOfApproved(rsI.getDate("date_of_approved"));
				image.setDateOfUpload(rsI.getDate("date_of_upload"));
				image.setDeleted(rsI.getInt("deleted"));
				image.setName(rsI.getString("name"));
				image.setLocation(rsI.getString("location"));
				image.setPrice(rsI.getInt("price"));
				image.setRating(rsI.getDouble("rating"));
				image.setUserId(rsI.getInt("id"));
				image.setVersion(rsI.getInt("version"));
				image.setPath(rsI.getString("path"));
				images.add(image);
			}
			return images;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Image> findByApprovedStatus(Integer status, int page, int size) {
		try {
			List<Image> images = new ArrayList<>();
			String QUERY = "SELECT * FROM image WHERE approved_status = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, status);
			ResultSet rsI = preparedStatement.executeQuery();
			while (rsI.next()) {
				Image image = new Image();
				image.setId(rsI.getInt("id"));
				image.setApprovedStatus(rsI.getInt("approved_status"));
				image.setDateOfApproved(rsI.getDate("date_of_approved"));
				image.setDateOfUpload(rsI.getDate("date_of_upload"));
				image.setDeleted(rsI.getInt("deleted"));
				image.setName(rsI.getString("name"));
				image.setLocation(rsI.getString("name"));
				image.setPrice(rsI.getInt("price"));
				image.setRating(rsI.getDouble("rating"));
				image.setUserId(rsI.getInt("id"));
				image.setVersion(rsI.getInt("version"));
				image.setPath(rsI.getString("path"));
				images.add(image);
			}
			return images;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public FullImageDto findById(Integer imageId) {
		try {
			Image image = findByImageId(imageId);
			if (image == null)
				return null;
			List<ImageResolution> resolutions = new ArrayList<>();
			String QUERY = "SELECT * FROM image_resolution WHERE image_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				ImageResolution imageResolution = new ImageResolution();
				imageResolution.setId(rs.getInt("id"));
				imageResolution.setImageId(imageId);
				imageResolution.setPath(rs.getString("path"));
				imageResolution.setPrice(rs.getInt("price"));
				imageResolution.setResolution(rs.getString("resolution"));
				imageResolution.setTotalSelled(rs.getInt("total_selled"));
				resolutions.add(imageResolution);
			}

			List<Category> categories = categoryDao.findByImageId(imageId);

			FullImageDto fullImageDto = new FullImageDto();
			fullImageDto.setImage(image);
			fullImageDto.setCategories(categories);
			fullImageDto.setResolutions(resolutions);
			return fullImageDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Image> findByUserId(Integer userId) {
		try {
			List<Image> images = new ArrayList<>();
			String QUERY = "SELECT * FROM image WHERE user_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, userId);
			ResultSet rsI = preparedStatement.executeQuery();
			while (rsI.next()) {
				Image image = new Image();
				image.setId(rsI.getInt("id"));
				image.setApprovedStatus(rsI.getInt("approved_status"));
				image.setDateOfApproved(rsI.getDate("date_of_approved"));
				image.setDateOfUpload(rsI.getDate("date_of_upload"));
				image.setDeleted(rsI.getInt("deleted"));
				image.setName(rsI.getString("name"));
				image.setLocation(rsI.getString("name"));
				image.setPrice(rsI.getInt("price"));
				image.setRating(rsI.getDouble("rating"));
				image.setUserId(userId);
				image.setVersion(rsI.getInt("version"));
				image.setPath(rsI.getString("path"));
				images.add(image);
			}
			return images;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Image findByImageId(Integer imageId) {
		try {
			String QUERY = "SELECT * FROM image WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageId);
			ResultSet rsI = preparedStatement.executeQuery();
			Image image = null;
			if (rsI.next()) {
				image = new Image();
				image.setId(rsI.getInt("id"));
				image.setApprovedStatus(rsI.getInt("approved_status"));
				image.setDateOfApproved(rsI.getDate("date_of_approved"));
				image.setDateOfUpload(rsI.getDate("date_of_upload"));
				image.setDeleted(rsI.getInt("deleted"));
				image.setName(rsI.getString("name"));
				image.setLocation(rsI.getString("location"));
				image.setPrice(rsI.getInt("price"));
				image.setRating(rsI.getDouble("rating"));
				image.setUserId(rsI.getInt("user_id"));
				image.setVersion(rsI.getInt("version"));
				image.setPath(rsI.getString("path"));
			}
			return image;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean updateApprovedStatus(Integer imageId, Integer status) {
		try {
			String QUERY = "UPDATE image SET approved_status = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, status);
			preparedStatement.setInt(2, imageId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean updateDeletedStatus(Integer imageId, Integer status) {
		try {
			String QUERY = "UPDATE image SET deleted = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, status);
			preparedStatement.setInt(2, imageId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Image save(Image image) {
		try {
			String QUERY = "INSERT INTO image (name, location, price, user_id,date_of_upload,approved_status) VALUES (?,?,?,?,?,?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, image.getName());
			preparedStatement.setString(2, image.getLocation());
			preparedStatement.setInt(3, image.getPrice());
			preparedStatement.setInt(4, image.getUserId());
			java.sql.Date d = new java.sql.Date(image.getDateOfUpload().getTime());
			preparedStatement.setDate(5, d);
			preparedStatement.setInt(6, image.getApprovedStatus());

			preparedStatement.execute();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				image.setId(genValue);
			}

			return image;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean updatePath(Image image) {
		try {
			String QUERY = "UPDATE image SET path = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setString(1, image.getPath());
			preparedStatement.setInt(2, image.getId());
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean removeFromCart(Integer imageId, Integer cartId) {
		try {
			String QUERY = "DELETE FROM cart_image WHERE image_id = ? AND cart_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageId);
			preparedStatement.setInt(2, cartId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void saveResolution(Resolution resolution) {
		try {
			String QUERY = "INSERT INTO resolution (resolution, count, price, photoId,path) VALUES (?,?,?,?,?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, resolution.getResType());
			preparedStatement.setInt(2, resolution.getCount());
			preparedStatement.setInt(3, resolution.getPrice());
			preparedStatement.setInt(4, resolution.getPhotoId());
			preparedStatement.setString(6, resolution.getPath());

			preparedStatement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getResolutionPathForRequestedImage(Integer photoId, String resolution) {
		try {
			String path = "";
			String QUERY = "SELECT * FROM resolution WHERE photoId = ? AND resolution = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, photoId);
			preparedStatement.setString(2, resolution);
			ResultSet rsI = preparedStatement.executeQuery();
			while (rsI.next()) {
				path = rsI.getString("path");
			}
			return path;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Rating> findRatingsByImageId(Integer imageId) {
		try {
			String QUERY = "SELECT * FROM rating WHERE image_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageId);
			ResultSet rs = preparedStatement.executeQuery();
			List<Rating> ratings = new ArrayList<>();
			while(rs.next()) {
				Rating rating = new Rating();
				rating.setRating(rs.getInt("rating"));
				ratings.add(rating);
			}
			return ratings;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
