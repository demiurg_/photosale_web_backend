package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.dao.ImageCategoryDao;
import rs.raf.web.webproj.domain.ImageCategory;

public class ImageCategoryDaoImpl implements ImageCategoryDao {
	
	private Connection connection;
	
	public ImageCategoryDaoImpl() {
		connection = DatabaseConfig.getConnection();
	}

	@Override
	public ImageCategory save(ImageCategory imageCategory) {
		try {
			String QUERY = "INSERT INTO image_category (image_id, category_id) VALUES (?, ?)";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageCategory.getImageId());
			preparedStatement.setInt(2, imageCategory.getCategoryId());
			preparedStatement.execute();
			return imageCategory;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ImageCategory> findByCategoryId(Integer imageId) {
		try {
			List<ImageCategory> imageCategories = new ArrayList<>();
			String QUERY = "SELECT * FROM image_category WHERE image_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, imageId);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				ImageCategory imageCategory = new ImageCategory();
				imageCategory.setId(rs.getInt("id"));
				imageCategory.setImageId(imageId);
				imageCategory.setCategoryId(rs.getInt("category_id"));
				imageCategories.add(imageCategory);
			}
			return imageCategories;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
