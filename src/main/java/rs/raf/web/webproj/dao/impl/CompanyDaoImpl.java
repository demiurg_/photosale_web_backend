package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.CompanyDao;
import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.domain.Company;

public class CompanyDaoImpl implements CompanyDao {
	
	private Connection connection;
	
	public CompanyDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
	}

	@Override
	public List<Company> findAll() {
		try {
			List<Company> companies = new ArrayList<>();
			String QUERY = "SELECT * FROM company";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				Company company = new Company();
				company.setId(rs.getInt("id"));
				company.setName(rs.getString("name"));
				company.setLocation(rs.getString("location"));
				company.setPib(rs.getString("pib"));
				company.setRang(rs.getString("rang"));
				company.setPercent(rs.getInt("percent"));
				company.setApprovedStatus(rs.getInt("approved_status"));
				companies.add(company);
			}
			return companies;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}

	@Override
	public List<Company> findByApprovedStatus(Integer status) {
		try {
			List<Company> companies = new ArrayList<>();
			String QUERY = "SELECT * FROM company where approved_status = "+status+";";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				Company company = new Company();
				company.setId(rs.getInt("id"));
				company.setName(rs.getString("name"));
				company.setLocation(rs.getString("location"));
				company.setPib(rs.getString("pib"));
				company.setRang(rs.getString("rang"));
				company.setPercent(rs.getInt("percent"));
				company.setApprovedStatus(rs.getInt("approved_status"));
				companies.add(company);
			}
			return companies;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}

	@Override
	public Boolean updateApprovedStatus(Integer companyId, Integer status) {
		try {
			String QUERY = "UPDATE company SET approved_status = ? WHERE id = ?;";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, status);
			preparedStatement.setInt(2, companyId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Company save(Company company) {
		try {
			String QUERY =  "INSERT INTO company (name, location, pib, rang, percent, approved_status) VALUES (?,?,?,?,?,?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, company.getName());
			preparedStatement.setString(2, company.getLocation());
			preparedStatement.setString(3, company.getPib());
			preparedStatement.setString(4, company.getRang());
			preparedStatement.setInt(5, company.getPercent());
			preparedStatement.setInt(6, company.getApprovedStatus());
			
			preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				company.setId(genValue);
			}
			
			return company;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean joinUserToCompany(Integer userId, Integer companyId) {
		try {
			String QUERY = "UPDATE user SET company_id = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, companyId);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
