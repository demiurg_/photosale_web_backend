package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.dao.UserDao;
import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.TestUser;
import rs.raf.web.webproj.domain.User;

public class UserDaoImpl implements UserDao {
	
	private Connection connection;
	
	public UserDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
	}

	@Override
	public List<User> findAll() {
		try {
			List<User> users = new ArrayList<>();
			String QUERY = "SELECT * FROM user";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
				users.add(user);
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<User> findAllSallers(int page, int size) {
		try {
			List<User> users = new ArrayList<>();
			String QUERY = "SELECT * FROM user WHERE role = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, 1);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
				users.add(user);
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User save(User user) {
		try {
			String QUERY =  "INSERT INTO user (username, password, email, role, deleted, verify, limit_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getEmail());
			preparedStatement.setInt(4, user.getRole());
			preparedStatement.setInt(5, user.getDeleted());
			preparedStatement.setInt(6, user.getVerify());
			java.sql.Date d = new java.sql.Date(user.getLimitDate().getTime());
			preparedStatement.setDate(7, d);
			preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				user.setId(genValue);
			}
			
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean setCompanyIdToUser(Integer userId, Integer companyId) {
		try {
			String QUERY = "UPDATE user SET company_id = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, companyId);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public TestUser saveTestUser(TestUser testUser) {
		try {
			String QYERY = "INSERT INTO test_user (user_id, count) VALUES (?, ?)";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QYERY);
			preparedStatement.setInt(1, testUser.getUserId());
			preparedStatement.setInt(2, testUser.getCount());
			preparedStatement.execute();
			return testUser;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<User> findByDeletedStatus(Integer deletedStatus) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByApprovedStatus(Integer approvedStatus) {
		try {
			List<User> users = new ArrayList<>();
			String QUERY = "SELECT * FROM user WHERE role = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, approvedStatus);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
				users.add(user);
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User findByEmailAndPassword(String email, String password) {
		try {
			String QUERY = "SELECT * FROM user WHERE email = ? AND password = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, password);
			ResultSet rs = preparedStatement.executeQuery();
			User user = null;
			if(rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<rs.raf.web.webproj.domain.dto.TestUser> findTestUsers() {
		try {
			List<rs.raf.web.webproj.domain.dto.TestUser> testUsers = new ArrayList<>();
			String QUERY_TEST_USERS = "SELECT * FROM test_user";
			PreparedStatement preparedStatementTestUsers = this.connection.prepareStatement(QUERY_TEST_USERS);
			ResultSet rs = preparedStatementTestUsers.executeQuery();
			List<Integer> userIds = new ArrayList<>();
			while(rs.next()) {
				userIds.add(rs.getInt("user_id"));
			}
			for(Integer userId: userIds) {
				List<Image> images = new ArrayList<>();
				String QUERY_IMAGES = "SELECT * FROM image WHERE user_id = ?";
				PreparedStatement preparedStatementImages = this.connection.prepareStatement(QUERY_IMAGES);
				preparedStatementImages.setInt(1, userId);
				ResultSet rsI = preparedStatementImages.executeQuery();
				while(rsI.next()) {
					Image image = new Image();
					image.setId(rsI.getInt("id"));
					image.setApprovedStatus(rsI.getInt("approved_status"));
					image.setDateOfApproved(rsI.getDate("date_of_approved"));
					image.setDateOfUpload(rsI.getDate("date_of_upload"));
					image.setDeleted(rsI.getInt("deleted"));
					image.setName(rsI.getString("name"));
					image.setLocation(rsI.getString("name"));
					image.setPrice(rsI.getInt("price"));
					image.setRating(rsI.getDouble("rating"));
					image.setUserId(userId);
					image.setVersion(rsI.getInt("version"));
					image.setPath(rsI.getString("path"));
					images.add(image);
				}
				String email = null;
				PreparedStatement prSt = this.connection.prepareStatement("select email from user where id = ?");
				prSt.setInt(1, userId);
				ResultSet rsEmail = prSt.executeQuery();
				if(rsEmail.next()) {
					email = rsEmail.getString("email");
				}
				rs.raf.web.webproj.domain.dto.TestUser testUser = new rs.raf.web.webproj.domain.dto.TestUser();
				testUser.setUserId(userId);
				testUser.setImages(images);
				testUser.setEmail(email);
				testUsers.add(testUser);
			}
			return testUsers;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User findByEmail(String email) {
		try {
			String QUERY = "SELECT * FROM user WHERE email = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setString(1, email);
			ResultSet rs = preparedStatement.executeQuery();
			User user = null;
			if(rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean updateVerifyStatus(Integer userId, Integer newStatus) {
		try {
			String QUERY = "UPDATE user SET verify = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, newStatus);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean updateRole(Integer userId, Integer newRole) {
		try {
			String QUERY = "UPDATE user SET role = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, newRole);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean changePassword(Integer userId, String password) {
		try {
			String QUERY = "UPDATE user SET password = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setString(1, password);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean deleteTestUser(Integer userId) {
		try {
			String QUERY = "DELETE FROM test_user WHERE user_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Boolean setCreditCard(Integer userId, String card) {
		try {
			String QUERY = "UPDATE user SET credit_card = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setString(1, card);
			preparedStatement.setInt(2, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public User findById(Integer userId) {
		try {
			User user = null;
			String QUERY = "SELECT * FROM user WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, userId);
			ResultSet rs = preparedStatement.executeQuery();
			if(rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setCompanyId(rs.getInt("company_id"));
				user.setCreditCard(rs.getString("credit_card"));
				user.setEmail(rs.getString("email"));
				user.setDeleted(rs.getInt("deleted"));
				user.setRole(rs.getInt("role"));
				user.setVerify(rs.getInt("version"));
				user.setVersion(rs.getInt("version"));
				user.setLimitDate(rs.getDate("limit_date"));
				user.setDailyCount(rs.getInt("daily_count"));
				user.setWeeklyCount(rs.getInt("weekly_count"));
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean resetUploadLimit(Integer userId, Date newDateLimit) {
		try {
			String QUERY = "UPDATE user SET limit_date = ?,daily_count = ?, weekly_count = ? WHERE id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			java.sql.Date d = new java.sql.Date(newDateLimit.getTime());
			preparedStatement.setDate(1, d);
			preparedStatement.setInt(2, 0);
			preparedStatement.setInt(3, 0);
			preparedStatement.setInt(4, userId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
