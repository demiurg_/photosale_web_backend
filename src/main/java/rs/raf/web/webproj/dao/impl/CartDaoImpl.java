package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.CartDao;
import rs.raf.web.webproj.dao.CartImageDao;
import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.dao.ImageDao;
import rs.raf.web.webproj.domain.Cart;
import rs.raf.web.webproj.domain.CartImage;
import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.dto.CartDto;

public class CartDaoImpl implements CartDao {
	
	private Connection connection;
	
	private CartImageDao cartImageDao;
	
	private ImageDao imageDao;
	
	public CartDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
		cartImageDao = new CartImageDaoImpl();
		imageDao = new ImageDaoImpl();
	}

	@Override
	public List<Cart> findAll() {
		try {
			List<Cart> carts = new ArrayList<>();
			String QUERY = "SELECT * FROM cart";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				Cart cart = new Cart();
				cart.setId(rs.getInt("id"));
				cart.setUserId(rs.getInt("user_id"));
				cart.setStatus(rs.getInt("status"));
				cart.setVersion(rs.getInt("version"));
				carts.add(cart);
			}
			return carts;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}

	@Override
	public Cart save(Cart cart) {
		try {
			String QUERY =  "INSERT INTO cart (user_id, status, version) VALUES (?,?,?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, cart.getUserId());
			preparedStatement.setInt(2, cart.getStatus());
			preparedStatement.setInt(3, cart.getVersion());
			
			preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				cart.setId(genValue);
			}
			
			return cart;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean setStatus(Integer cartId, Integer status) {
		try {
			String QUERY = "UPDATE cart SET status = ? WHERE id = ?;";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, status);
			preparedStatement.setInt(2, cartId);
			preparedStatement.execute();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public CartDto findActiveCart(Integer userId) {
		try {
			String QUERY = "SELECT * FROM cart where status= ? and user_id=?;";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, 1);
			preparedStatement.setInt(2, userId);
			ResultSet rs = preparedStatement.executeQuery();
			Integer cartId = null;
			if(rs.next()) {
				cartId = rs.getInt("id");
			}
			if(cartId == null) return null;
			List<CartImage> cartImages = cartImageDao.getCartImagesByCartId(cartId);
			List<Image> images = new ArrayList<>();
			for(CartImage cartImage: cartImages) {
				Image image = imageDao.findByImageId(cartImage.getImageId());
				if(image != null) {
					images.add(image);
				}
			}
			CartDto cartDto = new CartDto();
			cartDto.setCartId(cartId);
			cartDto.setImages(images);
			return cartDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}

	@Override
	public List<CartDto> findByUser(Integer userId) {
		try {
			List<CartDto> carts = new ArrayList<>();
			String QUERY_CARTS_USER = "SELECT * FROM cart WHERE user_id = ?";
			PreparedStatement preparedStatementCartsOfUser = this.connection.prepareStatement(QUERY_CARTS_USER);
			preparedStatementCartsOfUser.setInt(1, userId);
			ResultSet rsCarts = preparedStatementCartsOfUser.executeQuery();
			List<Integer> cartIds = new ArrayList<>();
			while(rsCarts.next()) {
				cartIds.add(rsCarts.getInt("id"));
			}
			for(Integer cartId: cartIds) {
				String QUERY_IMAGES_FROM_CART = "SELECT * FROM cart_image WHERE cart_id = ?";
				PreparedStatement preparedStatementImagesFromCart = this.connection.prepareStatement(QUERY_IMAGES_FROM_CART);
				preparedStatementCartsOfUser.setInt(1, cartId);
				ResultSet rsImagesFromCart = preparedStatementImagesFromCart.executeQuery();
				List<Integer> imageIds = new ArrayList<>();
				while(rsImagesFromCart.next()) {
					imageIds.add(rsImagesFromCart.getInt("image_id"));
				}
				List<Image> images = new ArrayList<>();
				for(Integer imageId: imageIds) {
					String QUERY_IMAGE_BY_ID = "SELECT * FROM image WHERE id = ?";
					PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY_IMAGE_BY_ID);
					preparedStatement.setInt(1, imageId);
					ResultSet rsI = preparedStatement.executeQuery();
					while(rsI.next()) {
						Image image = new Image();
						image.setId(rsI.getInt("id"));
						image.setApprovedStatus(rsI.getInt("approved_status"));
						image.setDateOfApproved(rsI.getDate("date_of_approved"));
						image.setDateOfUpload(rsI.getDate("date_of_upload"));
						image.setDeleted(rsI.getInt("deleted"));
						image.setName(rsI.getString("name"));
						image.setLocation(rsI.getString("location"));
						image.setPrice(rsI.getInt("price"));
						image.setRating(rsI.getDouble("rating"));
						image.setUserId(rsI.getInt("id"));
						image.setVersion(rsI.getInt("version"));
						image.setPath(rsI.getString("path"));
						images.add(image);
					}
				}
				CartDto cartDto = new CartDto();
				cartDto.setCartId(cartId);
				cartDto.setImages(images);
				carts.add(cartDto);
			}
			return carts;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Integer activeCartId(Integer userId) {
		try {
			String QUERY = "SELECT * FROM cart where status= ? and user_id=?;";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, 1);
			preparedStatement.setInt(2, userId);
			ResultSet rs = preparedStatement.executeQuery();
			Integer cartId = null;
			if(rs.next()) {
				cartId = rs.getInt("id");
			}
			return cartId;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
