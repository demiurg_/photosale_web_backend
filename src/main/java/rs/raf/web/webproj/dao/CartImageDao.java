package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.CartImage;

public interface CartImageDao {
	
	public CartImage save (CartImage cartImage);
	
	public List<CartImage> getCartImagesByCartId (Integer cartId);

}
