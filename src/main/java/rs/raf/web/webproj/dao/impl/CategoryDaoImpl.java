package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.CategoryDao;
import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.domain.Category;

public class CategoryDaoImpl implements CategoryDao {
	
	private Connection connection;
	
	public CategoryDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
	}

	@Override
	public List<Category> findAll() {
		try {
			List<Category> categories = new ArrayList<>();
			String QUERY = "SELECT * FROM category";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				Category category = new Category();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				categories.add(category);
			}
			return categories;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}
	
	public boolean addCategoriesToPhoto(Integer photoId, List<Integer> list) {
		try {
		String QUERY = "INSERT INTO image_category (category_id, image_id) VALUES (?, ?)";
		PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			for(Integer l: list) {
				preparedStatement.setInt(1, l);
				preparedStatement.setInt(2, photoId);
				preparedStatement.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Category> findByImageId(Integer imageId) {
		try {
			List<Category> categories = new ArrayList<>();
			List<Long> ids = new ArrayList<>();
			
			String QUERY = "SELECT * FROM image_category where image_id ="+imageId+"";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				long categoryId = 0;
				categoryId = (rs.getInt("category_id"));
				ids.add(categoryId);
			}
			
			for (Long long1 : ids) {
				QUERY = "SELECT * FROM category where id ="+long1+";";
				preparedStatement = this.connection.prepareStatement(QUERY);
				rs = preparedStatement.executeQuery();
				while(rs.next()) {
					Category category = new Category();
					category.setId(rs.getInt("id"));
					category.setName(rs.getString("name"));
					categories.add(category);
				}
			}
			
			return categories;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
	}
	}

	@Override
	public Category save(Category category) {
		try {
			String QUERY =  "INSERT INTO category (name) VALUES (?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, category.getName());
			
			preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				category.setId(genValue);
			}
			
			return category;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
