package rs.raf.web.webproj.dao;

import rs.raf.web.webproj.domain.Rating;

public interface RatingDao {
	
	public Rating save (Rating rating);

}
