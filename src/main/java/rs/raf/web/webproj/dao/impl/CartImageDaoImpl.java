package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.raf.web.webproj.dao.CartImageDao;
import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.domain.CartImage;

public class CartImageDaoImpl implements CartImageDao {
	
	private Connection connection;
	
	public CartImageDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
	}

	@Override
	public CartImage save(CartImage cartImage) {
		try {
			String QUERY =  "INSERT INTO cart_image (cart_id, image_id, resolution) VALUES (?,?,?);";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, cartImage.getCartId());
			preparedStatement.setInt(2, cartImage.getImageId());
			preparedStatement.setString(3, cartImage.getResolution());
			
			preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()) {
				Integer genValue = rs.getInt("GENERATED_KEY");
				cartImage.setId(genValue);
			}
			
			return cartImage;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<CartImage> getCartImagesByCartId(Integer cartId) {
		try {
			List<CartImage> cartImages = new ArrayList<>();
			String QUERY = "SELECT * FROM cart_image WHERE cart_id = ?";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, cartId);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				CartImage cartImage = new CartImage();
				cartImage.setId(rs.getInt("id"));
				cartImage.setCartId(rs.getInt("cart_id"));
				cartImage.setImageId(rs.getInt("image_id"));
				cartImage.setResolution(rs.getString("resolution"));
				cartImages.add(cartImage);
			}
			return cartImages;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
