package rs.raf.web.webproj.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;

import rs.raf.web.webproj.dao.DatabaseConfig;
import rs.raf.web.webproj.dao.RatingDao;
import rs.raf.web.webproj.domain.Rating;

public class RatingDaoImpl implements RatingDao {
	
	private Connection connection;
	
	public RatingDaoImpl() {
		this.connection = DatabaseConfig.getConnection();
	}

	@Override
	public Rating save(Rating rating) {
		try {
			String QUERY = "INSERT INTO rating (user_id, image_id, rating) VALUES (?, ?, ?)";
			PreparedStatement preparedStatement = this.connection.prepareStatement(QUERY);
			preparedStatement.setInt(1, rating.getUserId());
			preparedStatement.setInt(2, rating.getImageId());
			preparedStatement.setInt(3, rating.getRating());
			preparedStatement.execute();
			return rating;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
