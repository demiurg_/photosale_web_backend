package rs.raf.web.webproj.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConfig {
	
	private static final String HOST = "localhost";
	
	private static final Integer PORT = 3306;
	
	private static final String DB = "web_exam";
	
	private static final String USN = "root";
	
	private static final String PASS = "root";
	
	private Connection connection;
	
	private static DatabaseConfig instance = null;
	
	private DatabaseConfig() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String connectionPath = "jdbc:mysql://%s:%d/%s?user=%s&password=%s";
			connection = DriverManager.getConnection(String.format(connectionPath, HOST, PORT, DB, USN, PASS));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection () {
		if(instance == null) instance = new DatabaseConfig();
		return instance.connection;
	}

}
