package rs.raf.web.webproj.dao;

import java.util.Date;
import java.util.List;

import rs.raf.web.webproj.domain.TestUser;
import rs.raf.web.webproj.domain.User;

public interface UserDao {
	
	public List<User> findAll ();
	
	public User findById(Integer userId);
	
	public User findByEmail (String email);
	
	public Boolean updateVerifyStatus (Integer userId, Integer newStatus);
	
	public Boolean updateRole (Integer userId, Integer newRole);
	
	public List<User> findAllSallers (int page, int size);
	
	public User findByEmailAndPassword (String email, String password);
	
	public User save (User user);
	
	public Boolean setCompanyIdToUser (Integer userId, Integer companyId);
	
	public List<User> findByDeletedStatus (Integer deletedStatus);
	
	public List<User> findByApprovedStatus (Integer approvedStatus);
	
	public TestUser saveTestUser (TestUser testUser);
	
	public Boolean deleteTestUser (Integer userId);
	
	public Boolean changePassword (Integer userId, String password);
	
	public Boolean setCreditCard (Integer userId, String card);
	
	public List<rs.raf.web.webproj.domain.dto.TestUser> findTestUsers ();
	
	public Boolean resetUploadLimit(Integer userId, Date newDateLimit);

}
