package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.Rating;
import rs.raf.web.webproj.domain.Resolution;
import rs.raf.web.webproj.domain.dto.FullImageDto;
import rs.raf.web.webproj.domain.dto.ResolutionDto;

public interface ImageDao {
	
	public List<Image> findAll ();
	
	public List<Image> findByApprovedStatus (Integer status, int page, int size);
	
	public FullImageDto findById (Integer imageId);
	
	public List<Image> findByUserId (Integer userId);
	
	public Image findByImageId (Integer imageId);
	
	public Boolean updateApprovedStatus (Integer imageId, Integer status);
	
	public Boolean updateDeletedStatus (Integer imageId, Integer status);
	
	public Boolean removeFromCart (Integer imageId, Integer cartId);
	
	public Image save(Image image);
	
	public Boolean updatePath(Image image);

	public void saveResolution(Resolution resolution);

	public String getResolutionPathForRequestedImage(Integer photoId, String resolution);

	List<Image> findAllFiltered(String input, String searchCriteria, String sortCriteria, String order);
	
	public List<Rating> findRatingsByImageId (Integer imageId);

	
	
}
