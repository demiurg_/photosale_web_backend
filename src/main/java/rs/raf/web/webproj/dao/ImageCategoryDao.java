package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.ImageCategory;

public interface ImageCategoryDao {
	
	public ImageCategory save (ImageCategory imageCategory);
	
	public List<ImageCategory> findByCategoryId (Integer imageId);

}
