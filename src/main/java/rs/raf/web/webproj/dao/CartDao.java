package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.Cart;
import rs.raf.web.webproj.domain.dto.CartDto;

public interface CartDao {
	
	public List<Cart> findAll ();
	
	public Cart save (Cart cart);
	
	public Boolean setStatus (Integer cartId, Integer status);
	
	public CartDto findActiveCart (Integer userId);
	
	public Integer activeCartId (Integer userId);
	
	public List<CartDto> findByUser (Integer userId);

}
