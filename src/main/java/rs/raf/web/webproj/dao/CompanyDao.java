package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.Company;

public interface CompanyDao {
	
	public List<Company> findAll ();
	
	public List<Company> findByApprovedStatus (Integer status);
	
	public Boolean updateApprovedStatus (Integer companyId, Integer status);
	
	public Boolean joinUserToCompany (Integer userId, Integer companyId);
	
	public Company save (Company company);
	
}
