package rs.raf.web.webproj.dao;

import java.util.List;

import rs.raf.web.webproj.domain.Category;

public interface CategoryDao {
	
	public List<Category> findAll ();
	
	public List<Category> findByImageId (Integer imageId);
	
	public Category save (Category category);
	
	public boolean addCategoriesToPhoto(Integer photoId, List<Integer> list);

}
