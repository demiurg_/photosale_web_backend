package rs.raf.web.webproj.domain.dto;

import java.util.List;

import rs.raf.web.webproj.domain.Image;

public class TestUser {
	
	private Integer userId;
	
	private String email;
	
	private List<Image> images;
	
	public Integer getUserId() {
		return userId;
	}
	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<Image> getImages() {
		return images;
	}
	
	public void setImages(List<Image> images) {
		this.images = images;
	}

}
