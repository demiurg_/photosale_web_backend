package rs.raf.web.webproj.domain.dto;

import java.util.List;

import rs.raf.web.webproj.domain.Category;
import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.ImageResolution;

public class FullImageDto {
	
	private Image image;
	
	private List<ImageResolution> resolutions;
	
	private List<Category> categories;
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public List<ImageResolution> getResolutions() {
		return resolutions;
	}
	
	public void setResolutions(List<ImageResolution> resolutions) {
		this.resolutions = resolutions;
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
