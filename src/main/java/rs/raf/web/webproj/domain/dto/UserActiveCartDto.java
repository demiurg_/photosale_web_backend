package rs.raf.web.webproj.domain.dto;

import java.util.List;

import rs.raf.web.webproj.domain.ImageResolution;

public class UserActiveCartDto {
	
	private Integer userId;
	
	private List<ImageResolution> images;
	
	public Integer getUserId() {
		return userId;
	}
	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public List<ImageResolution> getImages() {
		return images;
	}
	
	public void setImages(List<ImageResolution> images) {
		this.images = images;
	}

}
