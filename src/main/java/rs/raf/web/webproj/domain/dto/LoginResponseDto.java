package rs.raf.web.webproj.domain.dto;

import rs.raf.web.webproj.domain.User;

public class LoginResponseDto {
	
	private User user;
	
	private Integer activeCartId;
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
	public Integer getActiveCartId() {
		return activeCartId;
	}
	
	public void setActiveCartId(Integer activeCartId) {
		this.activeCartId = activeCartId;
	}

}
