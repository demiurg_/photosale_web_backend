package rs.raf.web.webproj.domain.dto;

import java.util.ArrayList;
import java.util.List;

public class UploadImageDto {
	
	private String name;
	private String location;
	private Integer userId;
	private List<ResolutionDto> resolutionList;
	private List<Integer> categoryList = new ArrayList<>();
	
	public UploadImageDto() {
		super();
	}

	public UploadImageDto(String name, String location, Integer userId, List<ResolutionDto> resolutionList,
			List<Integer> categoryList) {
		super();
		this.name = name;
		this.location = location;
		this.userId = userId;
		this.resolutionList = resolutionList;
		this.categoryList = categoryList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<ResolutionDto> getResolutionList() {
		return resolutionList;
	}

	public void setResolutionList(List<ResolutionDto> resolutionList) {
		this.resolutionList = resolutionList;
	}

	public List<Integer> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Integer> categoryList) {
		this.categoryList = categoryList;
	}
	
	
	
}
