package rs.raf.web.webproj.domain;

public class ImageResolution {

	private Integer id;

	private Integer imageId;
	
	private String resolution;
	
	private String path;

	private Integer totalSelled;
	
	private Integer price;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getTotalSelled() {
		return totalSelled;
	}

	public void setTotalSelled(Integer totalSelled) {
		this.totalSelled = totalSelled;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
