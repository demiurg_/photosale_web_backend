package rs.raf.web.webproj.domain.dto;

public class ResolutionDto {
	private String name;
	private String resolution;
	private Integer price;
	private String base64;
	public ResolutionDto(String name, String resolution, Integer price, String base64) {
		super();
		this.name = name;
		this.resolution = resolution;
		this.price = price;
		this.base64 = base64;
	}
	public ResolutionDto() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	
	
}
