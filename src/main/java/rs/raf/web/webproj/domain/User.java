package rs.raf.web.webproj.domain;

import java.util.Date;

public class User {

	private Integer id;
	
	private String username;
	
	private String password;
	
	private String email;

	private String creditCard;

	private Integer companyId;
	
	private Integer role;
	
	private Integer deleted;
	
	private Integer verify;
	
	private Integer version;

	private Date limitDate;
	
	private Integer dailyCount;
	
	private Integer weeklyCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getDailyCount() {
		return dailyCount;
	}
	
	public void setDailyCount(Integer dailyCount) {
		this.dailyCount = dailyCount;
	}
	
	public Integer getWeeklyCount() {
		return weeklyCount;
	}
	
	public void setWeeklyCount(Integer weeklyCount) {
		this.weeklyCount = weeklyCount;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	public Integer getRole() {
		return role;
	}
	
	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Integer getVerify() {
		return verify;
	}

	public void setVerify(Integer verify) {
		this.verify = verify;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Date getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", creditCard=" + creditCard + ", companyId=" + companyId + ", deleted=" + deleted + ", verify="
				+ verify + ", version=" + version + ", limitDate=" + limitDate + "]";
	}

}
