package rs.raf.web.webproj.domain;

public class Resolution {
	
	private Integer id;
	private Integer photoId;
	private String resType;
	private String path;
	private Integer count;
	private Integer price;
	
	public Resolution() {
		super();
	}

	public Resolution(Integer id, Integer photoId, String resType, String path, Integer count, Integer price) {
		super();
		this.id = id;
		this.photoId = photoId;
		this.resType = resType;
		this.path = path;
		this.count = count;
		this.price = price;
	}
	public Resolution(Integer photoId, String resType, String path, Integer count, Integer price) {
		super();
		this.photoId = photoId;
		this.resType = resType;
		this.path = path;
		this.count = count;
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
}
