package rs.raf.web.webproj.domain.dto;

public class ImageDownloadInfo {
	private String resolution;
	private Integer photoId;
	private String name;
	private Integer price;
	
	public ImageDownloadInfo(String resolution, Integer photoId, String name, Integer price) {
		super();
		this.resolution = resolution;
		this.photoId = photoId;
		this.name = name;
		this.price = price;
	}
	public ImageDownloadInfo() {
		super();
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public Integer getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
}
