package rs.raf.web.webproj.domain.dto;

import java.util.List;

import rs.raf.web.webproj.domain.Image;

public class CartDto {
	
	private Integer cartId;
	
	private List<Image> images;
	
	public Integer getCartId() {
		return cartId;
	}
	
	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}
	
	public List<Image> getImages() {
		return images;
	}
	
	public void setImages(List<Image> images) {
		this.images = images;
	}

}
