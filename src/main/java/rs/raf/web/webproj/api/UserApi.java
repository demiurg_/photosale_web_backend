package rs.raf.web.webproj.api;

import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import rs.raf.web.webproj.domain.User;
import rs.raf.web.webproj.domain.dto.ChangePasswordDto;
import rs.raf.web.webproj.domain.dto.ImageDownloadInfo;
import rs.raf.web.webproj.domain.dto.LoginRequestDto;
import rs.raf.web.webproj.domain.dto.LoginResponseDto;
import rs.raf.web.webproj.domain.dto.TestUser;
import rs.raf.web.webproj.domain.dto.UserRegistrationDto;
import rs.raf.web.webproj.service.UserService;
import rs.raf.web.webproj.service.impl.UserServiceImpl;

@Path("user")
public class UserApi {
	
	private UserService userService;
	
	public UserApi() {
		userService = new UserServiceImpl();
	}
	
	@GET
    @Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
    public List<User> findAll () {
    	return userService.findAll();
    }
	
	@GET
    @Path("/test")
	@Produces(MediaType.TEXT_PLAIN)
    public String test () {
    	return "Zdravo!";
    }
	
	@GET
    @Path("/newSeller")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean newSaller (@QueryParam("userId") Integer userId) {
    	return userService.newSaller(userId);
    }
	
	@GET
    @Path("/companyApproveUser")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean companyApproveUser (@QueryParam("userId") Integer userId, @QueryParam("companyId") Integer companyId) {
    	return null;
    }
	
	@GET
    @Path("/findByApprovedStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> findByApprovedStatus (@QueryParam("approvedStatus") Integer approvedStatus) {
		return null;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/register")
	public User register (UserRegistrationDto userRegistrationDto) {
		return userService.register(userRegistrationDto);
	}
	
	@GET
    @Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Boolean verify (@QueryParam("token") String token) {
		return userService.verify(token);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/login")
	public LoginResponseDto login (LoginRequestDto loginRequestDto) {
		return userService.login(loginRequestDto);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sendPurchasedPhotoToUserMail")
	public Boolean sendPurchasedPhotoToUserMail(ImageDownloadInfo imageDownloadInfo) throws MessagingException {
		return userService.sendPurchasedPhotoToUserMail(imageDownloadInfo);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/resetPassword")
	public Boolean resetPassword (ChangePasswordDto changePasswordDto) {
		return userService.changePassword(changePasswordDto);
	}
	
	@GET
    @Path("/forgotPassword")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean forgotPassword (@QueryParam("email") String email) {
    	return userService.forgotPassword(email);
    }
	
	@GET
    @Path("/checkUploadLimit")
	@Produces(MediaType.APPLICATION_JSON)
    public Boolean checkUploadLimit (@QueryParam("userId") Integer userId) {
    	return userService.checkUploadLimit(userId);
    }
	
	@GET
    @Path("/addCreditCard")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean addCreditCard (@QueryParam("userId") Integer userId, @QueryParam("card") String card) {
    	return userService.addCreditCard(userId, card);
    }
	
	@GET
    @Path("/findTestUsers")
	@Produces(MediaType.APPLICATION_JSON)
    public List<TestUser> findTestUsers () {
    	return userService.findTestUsers();
    }
	
	

}
