package rs.raf.web.webproj.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import rs.raf.web.webproj.domain.Image;
import rs.raf.web.webproj.domain.dto.FullImageDto;
import rs.raf.web.webproj.domain.dto.UploadImageDto;
import rs.raf.web.webproj.service.ImageService;
import rs.raf.web.webproj.service.impl.ImageServiceImpl;

@Path("image")
public class ImageApi {
	
	private ImageService imageService;
	
	public ImageApi() {
		imageService = new ImageServiceImpl();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/upload")
	public Image upload (UploadImageDto uploadImageDto) {
		return imageService.upload(uploadImageDto);
	}
	
	@GET
    @Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Image> findAll() {
    	return imageService.findAll();
    }
	
	@GET
    @Path("/findByApprovedStatus")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Image> findByApprovedStatus(@QueryParam("status") Integer status) {
    	return imageService.findByApprovedStatus(status, 0, 0);
    }
	
	@GET
    @Path("/findByUser")
	@Produces(MediaType.APPLICATION_JSON)
    public List<FullImageDto> findByUser(@QueryParam("userId") Integer userId) {
    	return imageService.findByUser(userId);
    }
	
	@GET
    @Path("/findByCategory")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Image> findByCategory(@QueryParam("categoryId") Integer categoryId) {
    	return imageService.findByCategory(categoryId);
    }
	
	@GET
    @Path("/approveImage")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean approveImage (@QueryParam("imageId") Integer imageId) {
    	return imageService.approveImage(imageId);
    }
	
	@GET
    @Path("/deleteImage")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean deleteImage (@QueryParam("imageId") Integer imageId) {
    	return imageService.deleteImage(imageId);
    }
	
	@GET
    @Path("/searchImages")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Image> searchImages (@QueryParam("size") Integer size, @QueryParam("page") Integer page) {
    	return imageService.searchImages(size, page);
    }
	
	@GET
    @Path("/addToCart")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean addToCart (@QueryParam("imageId") Integer imageId, @QueryParam("cartId") Integer cartId) {
    	return imageService.addToCart(imageId, cartId);
    }
	
	@GET
    @Path("/removeFromCart")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean removeFromCart (@QueryParam("imageId") Integer imageId, @QueryParam("cartId") Integer cartId) {
    	return imageService.removeFromCart(imageId, cartId);
    }
	
	@GET
    @Path("/rateImage")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean rateImage (@QueryParam("imageId") Integer imageId, @QueryParam("userId") Integer userId, @QueryParam("rating") Integer rating) {
    	return imageService.rateImage(imageId, userId, rating);
    }
	
	@GET
    @Path("/fetchRating")
	@Produces(MediaType.TEXT_PLAIN)
    public Double fetchRating (@QueryParam("imageId") Integer imageId) {
    	return imageService.fetchRating(imageId);
    }

}
