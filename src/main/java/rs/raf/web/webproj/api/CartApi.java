package rs.raf.web.webproj.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import rs.raf.web.webproj.domain.Cart;
import rs.raf.web.webproj.domain.ImageResolution;
import rs.raf.web.webproj.domain.dto.CartDto;
import rs.raf.web.webproj.service.CartService;
import rs.raf.web.webproj.service.impl.CartServiceImpl;

@Path("cart")
public class CartApi {
	
	private CartService cartService;
	
	public CartApi() {
		cartService = new CartServiceImpl();
	}
	
	@GET
    @Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Cart> findAll() {
    	return cartService.findAll();
    }
	
	@GET
    @Path("/findByUser")
	@Produces(MediaType.APPLICATION_JSON)
    public List<CartDto> findByUser(@QueryParam("userId") Integer userId) {
    	return cartService.findByUser(userId);
    }
	
	@GET
    @Path("/openCart")
	@Produces(MediaType.APPLICATION_JSON)
    public Cart openCart(@QueryParam("userId") Integer userId) {
    	return cartService.openCart(userId);
    }
	
	@GET
    @Path("/closeCart")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean closeCart(@QueryParam("cartId") Integer cartId) {
    	return cartService.closeCart(cartId);
    }
	
	@GET
    @Path("/findContentForActiveCart")
	@Produces(MediaType.APPLICATION_JSON)
    public CartDto findActiveCart (@QueryParam("userId") Integer userId) {
    	return cartService.findActiveCartByUser(userId);
    }
	
	@GET
    @Path("/findContentByCart")
	@Produces(MediaType.APPLICATION_JSON)
    public List<ImageResolution> findContentByCart(@QueryParam("cartId") Integer cartId) {
    	return null;
    }

}
