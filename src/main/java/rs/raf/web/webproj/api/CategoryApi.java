package rs.raf.web.webproj.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import rs.raf.web.webproj.domain.Category;
import rs.raf.web.webproj.service.CategoryService;
import rs.raf.web.webproj.service.impl.CategoryServiceImpl;

@Path("category")
public class CategoryApi {
	
	private CategoryService categoryService;
	
	public CategoryApi() {
		categoryService = new CategoryServiceImpl();
	}
	
	@GET
    @Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Category> findAll() {
    	return categoryService.findAll();
    }
	
	@GET
    @Path("/findCategoriesForImage")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Category> findCategoriesForImage(@QueryParam("imageId") Integer imageId) {
    	return categoryService.findCategoriesForImage(imageId);
    }
	
	@GET
    @Path("/createNew")
	@Produces(MediaType.APPLICATION_JSON)
    public Category createNew(@QueryParam("name") String name) {
    	return categoryService.createNew(name);
    }

}
