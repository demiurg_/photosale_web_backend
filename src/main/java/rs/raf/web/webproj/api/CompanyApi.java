package rs.raf.web.webproj.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import rs.raf.web.webproj.domain.Company;
import rs.raf.web.webproj.service.CompanyService;
import rs.raf.web.webproj.service.impl.CompanyServiceImpl;

@Path("company")
public class CompanyApi {
	
	private CompanyService companyService;
	
	public CompanyApi() {
		companyService = new CompanyServiceImpl();
	}
	
	@GET
    @Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Company> findAll() {
    	return companyService.findAll();
    }
	
	@GET
    @Path("/findByApprovedStatus")
	@Produces(MediaType.APPLICATION_JSON)
    public List<Company> findByApprovedStatus (@QueryParam("status") Integer status) {
    	return companyService.findByApprovedStatus(status);
    }
	
	@GET
    @Path("/joinCompany")
	@Produces(MediaType.APPLICATION_JSON)
    public Boolean joinCompany(@QueryParam("companyId") Integer companyId, @QueryParam("userId") Integer userId) {
    	return companyService.joinCompany(companyId, userId);
    }
	
	@GET
    @Path("/approveCompany")
	@Produces(MediaType.TEXT_PLAIN)
    public Boolean approveCompany(@QueryParam("companyId") Integer companyId) {
    	return companyService.approveCompany(companyId);
    }
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/registerCompany")
	public Company registerCompany (Company company) {
		return companyService.registerCompany(company);
	}

}
